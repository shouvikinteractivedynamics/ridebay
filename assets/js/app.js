const myNav = document.querySelector('.nav');
const burger = document.querySelector('.nav_menu');
const cross = document.querySelector('.cross');
const sideNav = document.querySelector('.sidenav');

window.onscroll = function () { 
    if (document.body.scrollTop >= 200 || document.documentElement.scrollTop >= 200 ) {
        myNav.classList.add("nav-colored");
    } 
    else {
        myNav.classList.remove("nav-colored");
    }
};

burger.addEventListener('click',()=>{
    sideNav.classList.add("trigger");
});

cross.addEventListener('click', ()=>{
    sideNav.classList.remove("trigger");
})